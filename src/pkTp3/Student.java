package pkTp3;

import org.w3c.dom.NamedNodeMap;
import pkList.ListNode;

import java.io.Serializable;

/**
 * Created by Nicolas Boisvert on 2016-03-31.
 */
public class Student implements Serializable{
    String permanent_code; 		//	code permanent,
    String lastname; 			//	nom,
    String firstname; 			//	prénom,
    int program_code; 			//	numéro du programme,
    int credits; 				//	nombre de crédits accumulés,
    double average; 			//	moyenne cumulative,
    ListNode suscribed_courses; 		//	la liste des cours auxquels il s’est inscrit (R2),   Remplacer type par la liste
    int suscribed_courses_count;//	nombre de cours auxquels il est inscrit.             Remplacer type par la liste

    public Student(String permanent_code,String lastname,String firstname,int program_code,int credits,ListNode suscribed_courses,int suscribed_courses_count){
        this.permanent_code = permanent_code;
        this.lastname = lastname;
        this.firstname = firstname;
        this.program_code = program_code;
        this.credits = credits;
        this.suscribed_courses = suscribed_courses;
        this.suscribed_courses_count = suscribed_courses_count;
    }

    public Student(String permanent_code,String lastname,String firstname,int program_code,int credits){
        this.permanent_code = permanent_code;
        this.lastname = lastname;
        this.firstname = firstname;
        this.program_code = program_code;
        this.credits = credits;
        this.suscribed_courses = null;
        this.suscribed_courses_count = 0;
    }

    public String getPermanent_code() {
        return permanent_code;
    }

    public void setPermanent_code(String permanent_code) {
        this.permanent_code = permanent_code;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public int getProgram_code() {
        return program_code;
    }

    public void setProgram_code(int program_code) {
        this.program_code = program_code;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public ListNode getSuscribedCourses() {
        return suscribed_courses;
    }

    public void setSuscribedCourses(ListNode suscribed_courses) {
        this.suscribed_courses = suscribed_courses;
    }

    public int getSuscribed_courses_count() {
        return suscribed_courses_count;
    }

    public void setSuscribed_courses_count(int suscribed_courses_count) {
        this.suscribed_courses_count = suscribed_courses_count;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public static Student fromXml(NamedNodeMap node){
        String permanent_code = node.getNamedItem("permanent_code").getTextContent();
        String lastname = node.getNamedItem("lastname").getTextContent();
        String firstname = node.getNamedItem("firstname").getTextContent();
        int program_code = Integer.parseInt(node.getNamedItem("program_code").getTextContent());
        int credits = Integer.parseInt(node.getNamedItem("credits").getTextContent());
        double average = Double.parseDouble(node.getNamedItem("average").getTextContent());
        return new Student(permanent_code,lastname,firstname,program_code,credits);
    }

    public String toString(){
        return "permanent_code = "+permanent_code+"\n" +
                "lastname = "+lastname+"\n" +
                "firstname = "+firstname+"\n" +
                "program_code = "+program_code+"\n" +
                "credits = "+credits+"\n";
    }
}
