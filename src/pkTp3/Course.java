package pkTp3;

import java.io.Serializable;
import org.w3c.dom.NamedNodeMap;
import pkList.ListNode;

/**
 * Created by Nicolas Boisvert on 2016-04-02.
 */
public class Course implements Serializable {
    String acronym;			//	le sigle du cours,
    String name;			//	le nom du cours,
    int maximum_students;	//	nombre maximum d’étudiants,
    String prerequisites;	//	une liste des pré-requis,
    int students_count;		//	nombre d’étudiants inscrits,
    transient ListNode students;			//	la liste des étudiants inscrits à ce cours (R1).    Remplacer type par la liste

    public Course(String acronym,String name,int maximum_students,int students_count,ListNode students){
        this.acronym = acronym;
        this.name = name;
        this.maximum_students = maximum_students;
        this.students_count = students_count;
        this.students = students;
    }

    public Course(String acronym,String name,int maximum_students){
        this.acronym = acronym;
        this.name = name;
        this.maximum_students = maximum_students;
        this.students_count = students_count;
        this.students = null;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaximum_students() {
        return maximum_students;
    }

    public void setMaximum_students(int maximum_students) {
        this.maximum_students = maximum_students;
    }

    public int getStudents_count() {
        return students_count;
    }

    public void setStudents_count(int students_count) {
        this.students_count = students_count;
    }

    public ListNode getStudents() {
        return students;
    }

    public void setStudents(ListNode students) {
        this.students = students;
    }

    public String getPrerequisites() {
        return prerequisites;
    }

    public void setPrerequisites(String prerequisites) {
        this.prerequisites = prerequisites;
    }

    public static Course fromXml(NamedNodeMap node){
        String acronym = ((node.getNamedItem("acronym") != null) ? node.getNamedItem("acronym").getTextContent() : "");
        String name = ((node.getNamedItem("name") != null) ? node.getNamedItem("name").getTextContent() : "");
        int maximum_students = (node.getNamedItem("maximum_students").getTextContent() != null) ? Integer.parseInt(node.getNamedItem("maximum_students").getTextContent()) : -1;
        return new Course(acronym,name,maximum_students);
    }

    public String toString(){
        return "acronym = "+acronym+"\n" +
                "name = "+name+"\n" +
                "maximum_students = "+maximum_students+"\n" +
                "students_count = "+students_count+"\n";
    }
}
