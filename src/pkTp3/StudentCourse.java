package pkTp3;

/**
 * Created by Nicolas Boisvert on 2016-04-02.
 */
public class StudentCourse {
    int student_id ; 			//	indice de l’étudiant dans le tableau étudiant,
    int course_id ; 			//	indice du cours dans le tableau des cours,
    int suscribed_students ; 	//	une référence pour référencer tous les étudiants inscrits à ce cours,
    int suscribed_course ; 		//	une référence  pour référencer tous les cours suivis par un étudiant donné.


    public StudentCourse(int student_id,int course_id,int suscribed_students,int suscribed_course){
        this.student_id = student_id;
        this.course_id = course_id;
        this.suscribed_students = suscribed_students;
        this.suscribed_course = suscribed_course;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public int getSuscribed_students() {
        return suscribed_students;
    }

    public void setSuscribed_students(int suscribed_students) {
        this.suscribed_students = suscribed_students;
    }

    public int getSuscribed_course() {
        return suscribed_course;
    }

    public void setSuscribed_course(int suscribed_course) {
        this.suscribed_course = suscribed_course;
    }
}
