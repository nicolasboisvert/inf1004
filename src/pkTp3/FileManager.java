/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkTp3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author moysk
 */
public class FileManager{
     private static FileInputStream openFileInput(String name) {
        try {
            return new java.io.FileInputStream(name);
        } catch (FileNotFoundException ex) {
        }
        return null;
    }

    private static FileOutputStream openFileOutput(String name) {
        try {
           return new java.io.FileOutputStream(name);
        } catch (FileNotFoundException ex) {
        }
        return null;
    }

    private static ObjectInputStream getObjectInput(FileInputStream r) {
        try {
            return new ObjectInputStream(r);
        } catch (IOException ex) {
        }
        return null;
    }

    private static ObjectOutputStream getObjectOutput(FileOutputStream r) {
        try {
            return new ObjectOutputStream(r);
        } catch (IOException ex) {
        }
        return null;
    }

    private static boolean exists(String name) {
        return new File(name).isFile();
    }

    public static List getList(String filename) {

        List modeles = new LinkedList();
        if (exists(filename)) {
            try {
                try (FileInputStream inp = openFileInput(filename)) {
                    ObjectInputStream stream = getObjectInput(inp);
                    modeles=(List)stream.readObject();
                }
            } catch (IOException | ClassNotFoundException ex) {
            }
        }
        return modeles;

    }

    public static void saveModeles(List modeles, String filename) {
        try {
            try (FileOutputStream out = openFileOutput(filename)) {
                ObjectOutputStream stream = getObjectOutput(out);
                stream.writeObject(modeles);
            }
        } catch (IOException ex) {
        }
    }

}
