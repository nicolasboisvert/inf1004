/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkList;

import pkTp3.Course;
import pkTp3.Student;

/**
 *
 * @author moysk
 */
public class ListManager {

    public static void add(Student s, Course c,int s_id,int c_id)
    {
        ListNode ln = new ListNode(c_id,s_id);
        ln.setNextCourse(s.getSuscribedCourses());
        s.setSuscribedCourses(ln);
        ln.setNextStudent(c.getStudents());
        c.setStudents(ln);
    }
}
