/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkList;

import java.io.Serializable;

/**
 *
 * @author moysk
 */
public class ListNode implements Serializable{
    private transient ListNode nextStudent;
    private ListNode nextCourse;
    private int student;
    private int course;

    ListNode(int c, int s) {    
        student=s;
        course=c;
    }

    /**
     * @return the student
     */
    public int getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(int student) {
        this.student = student;
    }

    /**
     * @return the course
     */
    public int getCourse() {
        return course;
    }

    /**
     * @param course the course to set
     */
    public void setCourse(int course) {
        this.course = course;
    }

    /**
     * @return the nextStudent
     */
    public ListNode getNextStudent() {
        return nextStudent;
    }

    /**
     * @param nextStudent the nextStudent to set
     */
    public void setNextStudent(ListNode nextStudent) {
        this.nextStudent = nextStudent;
    }

    /**
     * @return the nextCourse
     */
    public ListNode getNextCourse() {
        return nextCourse;
    }

    /**
     * @param nextCourse the nextCourse to set
     */
    public void setNextCourse(ListNode nextCourse) {
        this.nextCourse = nextCourse;
    }
    
    
}
