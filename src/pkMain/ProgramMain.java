/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkMain;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import pkList.ListManager;
import pkList.ListNode;
import pkTp3.Course;
import pkTp3.Student;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import pkTp3.FileManager;

/**
 *
 * @author moysk
 */
public class ProgramMain {

    
    static List<Student> Students = new ArrayList<>();
    static List<Course> Courses = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        

        Scanner scan = new Scanner(System.in);
        // TODO code application logic here
        /*  •	Inscription d’un étudiant à un cours,
            •	Annulation de l’inscription d’un étudiant à un cours,
            •	Modification de l’inscription : annuler une inscription à un cours et le réinscrire à un autre cours,
            •	Obtention de la liste des cours suivis par l’étudiant,
            •	Obtention de la liste des étudiants inscrits à un cours donné.
            •	Sauvegarde des informations relatives aux étudiants, aux cours et aux inscriptions dans des fichiers.
            •	Lecture des informations relatives aux étudiants, aux cours et aux inscriptions à partir des  fichiers.
            */
        int menu=0;
        final int EXIT = 8;
        do
        {
            showMenu();
            menu = Integer.parseInt(scan.nextLine());
            switch(menu)
            {
                case 1://Inscription d’un étudiant à un cours,
                {
                    Student student = chooseStudent(Students,scan);
                    List<Course> available = availableForStudent(student,Courses);
                    chooseCourseToAddToStudent(student,available,scan);
                }break;
                case 2://Annulation de l’inscription d’un étudiant à un cours
                {
                    Student student = chooseStudent(Students,scan);
                    chooseCourseToRemoveFromStudent(student,scan);
                }break;
                case 3://Modification de l’inscription : annuler une inscription à un cours et le réinscrire à un autre cours
                {
                    Student student = chooseStudent(Students,scan);
                    chooseCourseToRemoveFromStudent(student,scan);
                    List<Course> available = availableForStudent(student,Courses);
                    chooseCourseToAddToStudent(student,available,scan);
                }break;
                case 4://Obtention de la liste des cours suivis par l’étudiant
                {
                    Student student = chooseStudent(Students,scan);
                    showSubscribedCourseForStudent(student);
                }break;
                case 5://Obtention de la liste des étudiants inscrits à un cours donné,
                {
                    Course course = chooseCourse(Courses,scan);
                    showStudentForCourse(course);
                }break;
                case 6://Sauvegarde des informations relatives aux étudiants, aux cours et aux inscriptions dans des fichiers,
                {
                    saveToFile();
                }break;
                case 7://Lecture des informations relatives aux étudiants, aux cours et aux inscriptions à partir des  fichiers,
                {
                    loadFromFile();
                }break;
            }
        }while(menu != EXIT);
    }

      public static void loadFromFile() {
        
        Students = FileManager.getList("students.bin");
        Courses = FileManager.getList("courses.bin");
        for(int i=0;i<Students.size();i++)
        {
            Student student = Students.get(i);
            ListNode node = student.getSuscribedCourses();
            student.setSuscribedCourses(null);
            while(node!=null)
            {
                int courseID = node.getCourse();
                int studentID = node.getStudent();
                ListManager.add(student, Courses.get(courseID), studentID, courseID);
                node = node.getNextCourse();
            }
        }
        
        if(Students.isEmpty())
        {
           
            Students.add(new Student("KELV9501010101","Patterson","Jeff",0,0));
            Students.add(new Student("KELV9501010101","Leef","John",2,0));
            Students.add(new Student("KELV9501010101","Green","Nic",2,0));
        }
        if(Courses.isEmpty())
        {
            Courses.add(new Course("INF-1004","Structures",20));
            Courses.add(new Course("INF-1005","Analyse",20));
            Courses.add(new Course("INF-1006","Conception",20));
        }
        //Load Students
        //Load Cources
        //Foreach student add subscription by number
    }

    public static void saveToFile() {
        FileManager.saveModeles(Courses, "courses.bin");
        FileManager.saveModeles(Students, "students.bin");
        //Save subscription by number in student
    }

    public static boolean isStudentSubscribed(Student s, Course c) {
        ListNode ln = c.getStudents();
        while (ln != null) {
            if (ln.getStudent() == Students.indexOf(s)) {
                return true;
            }
            ln = ln.getNextStudent();
        }
        return false;
    }

    public static List<Course> availableForStudent(Student s, List<Course> allCourses) {
        LinkedList<Course> courses = new LinkedList<>();
        for (int i = 0; i < allCourses.size(); i++) {
            Course c = allCourses.get(i);
            if (!isStudentSubscribed(s, c)) {
                courses.add(c);
            }
        }
        return courses;
    }

    public static void addSubscription(Student s, Course c) {
        ListManager.add(s, c,Students.indexOf(s),Courses.indexOf(c));
    }

    public static void showSubscribedCourseForStudent(Student s) {
        ListNode ln = s.getSuscribedCourses();
        while (ln != null) {
            System.out.println(Courses.get(ln.getCourse()));
            ln = ln.getNextCourse();
        }
    }

    public static void showStudentForCourse(Course c) {
        ListNode ln = c.getStudents();
        while (ln != null) {
            System.out.println(Students.get(ln.getCourse()));
            ln = ln.getNextStudent();
        }
    }

    private static void showMenu() {
        System.out.println("1)	Inscription d’un étudiant à un cours,\n" +
                            "2)	Annulation de l’inscription d’un étudiant à un cours,\n" +
                            "3)	Modification de l’inscription : annuler une inscription à un cours et le réinscrire à un autre cours,\n" +
                            "4)	Obtention de la liste des cours suivis par l’étudiant,\n" +
                            "5)	Obtention de la liste des étudiants inscrits à un cours donné.\n" +
                            "6)	Sauvegarde des informations relatives aux étudiants, aux cours et aux inscriptions dans des fichiers.\n" +
                            "7)	Lecture des informations relatives aux étudiants, aux cours et aux inscriptions à partir des  fichiers.\n"+
                            "8) Quitter");
    }

    private static Student chooseStudent(List<Student> Students,Scanner scan) {
        if(Students.size()==0)
            return null;
        for(int i=0;i<Students.size();i++)
        {
            Student student = Students.get(i);
            System.out.println((i+1) + " : "+student.getPermanent_code()+ " "+student.getLastname()+", "+student.getFirstname());
        }
        
        Student chosen = Students.get(Integer.parseInt(scan.nextLine())-1);
        return chosen;
    }

    private static void chooseCourseToAddToStudent(Student student, List<Course> available,Scanner scan) {
        
        Course chosen = chooseCourse(available,scan);
        addSubscription(student,chosen);
    }

    private static void chooseCourseToRemoveFromStudent(Student student,Scanner scan) {
       List<Course> courses = new ArrayList<>();
        ListNode ln = student.getSuscribedCourses();
        
        while (ln != null) {
            courses.add(Courses.get(ln.getCourse()));
            ln = ln.getNextCourse();
        }
        
        for(int i=0;i<courses.size();i++)
        {
            Course course = courses.get(i);
            System.out.println((i+1) + " : "+course.getAcronym()+ " "+course.getName());
        }
        
        Course toRemove = courses.get(Integer.parseInt(scan.nextLine())-1);
        int positionCourse = Courses.indexOf(toRemove);
        
        ListNode current = student.getSuscribedCourses();
        ListNode previous = null;
        while (current != null) {
           previous = current;
           if(current.getCourse() == positionCourse)
           {
               if(previous==null)
               {
                   student.setSuscribedCourses(current.getNextCourse());
               }
               else
               {
                   previous.setNextCourse(current.getNextCourse());
               }
           }
           ln = ln.getNextCourse();
        }
        
        current = toRemove.getStudents();
        previous = null;
        while (current != null) {
           previous = current;
           if(current.getCourse() == positionCourse)
           {
               if(previous==null)
               {
                   toRemove.setStudents(current.getNextStudent());
               }
               else
               {
                   previous.setNextStudent(current.getNextStudent());
               }
           }
           ln = ln.getNextStudent();
        }
        
    }

    private static Course chooseCourse(List<Course> courses,Scanner scan) {
        if(courses.isEmpty())
            return null;
       for(int i=0;i<courses.size();i++)
        {
            Course course = courses.get(i);
            System.out.println((i+1) + " : "+course.getAcronym()+ " "+course.getName());
        }
        
        Course chosen = courses.get(Integer.parseInt(scan.nextLine())-1);
        return chosen;
    }
}
